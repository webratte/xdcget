"""
Functionality to request, update, cache and export webxdc app files.
"""

import copy
import json
import shutil

import requests
import toml
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache

from .asset import (
    ZipFile,
    check_xdc_consistency,
    export_for_xdcstore,
    parse_manifest_and_icon,
)
from .errors import NoReleases, TokenError


class IndexEntry:
    """Data object with all information about an app entry in the application index."""

    def __init__(
        self,
        app_id,
        tag_name,
        url,
        date,
        description,
        source_code_url="",
        name="",
        cache_relname=None,
    ):
        self.app_id = app_id
        self.tag_name = tag_name
        self.url = url
        self.date = date
        self.description = description
        self.source_code_url = source_code_url
        self.name = name
        if cache_relname is None:
            cache_relname = f"{app_id}-{tag_name}.xdc"
        self.cache_relname = cache_relname

    def __eq__(self, other):
        return self.__dict__ == getattr(other, "__dict__", None)


class Index:
    """Persistent Index of latest application releases ("xdcget.lock").

    An Index is read and persisted to the configured xdcget.lock file.
    It provides methods to maintain an up-to-date view of
    all source apps configured in the xdcget.ini file.
    """

    def __init__(self, config):
        self.config = config  # Contents of the xdcget.ini file
        if config.index_path.exists():
            with config.index_path.open("r") as f:
                self._data = toml.load(f)
        else:
            self._data = {}
        self._original_data = copy.deepcopy(self._data)
        # Prune apps from the index not defined in our app sources list.
        for app_id in set(self._data) - set(x.app_id for x in self.config.source_defs):
            del self._data[app_id]

    def persist_if_modified(self):
        """Rewrite the xdcget.lock file if the Index changed."""
        if self._data != self._original_data:
            path = self.config.index_path
            tmp = path.with_name(path.name + ".tmp")
            with tmp.open("w") as f:
                toml.dump(self._data, f)
            tmp.rename(path)
            return True

        return False

    def set_entry(self, index_entry: IndexEntry):
        """Set or update an IndexEntry."""
        assert "." not in index_entry.app_id
        assert index_entry.name, "name attribute can not be empty"
        # write out a stably-ordered set of attributes for xdcget.lock entries
        self._data[index_entry.app_id] = dict(
            app_id=index_entry.app_id,
            name=index_entry.name,
            tag_name=index_entry.tag_name,
            url=index_entry.url,
            date=index_entry.date,
            cache_relname=index_entry.cache_relname,
            description=index_entry.description,
            source_code_url=index_entry.source_code_url,
        )

    def get_entry(self, app_id):
        """Get an IndexEntry for the specified app_id."""
        release = self._data.get(app_id)
        if release is not None:
            assert app_id == release["app_id"]
            return IndexEntry(**release)

    def iter_entries(self):
        return (self.get_entry(app_id) for app_id in self._data)

    def update_from_remote_repositories(self, out, app_filter, offline):
        """Update the application index by querying remote repositories
        for latest releases, also downloading any latest webxdc asset files.

        Args:
            config: configuration read from xdcget.ini
            app_filter: if not None, only update the sources
                whose app-id contains this string.
            out: output printer logging progress and problems.
            offline: if True, don't perform any network requests
        """
        config = self.config
        config.cache_dir.mkdir(exist_ok=True, parents=True)
        cached_session = CacheControl(
            requests.Session(), cache=FileCache(config.cache_dir / "web_cache")
        )
        if not offline:
            for api_def in self.config.api_defs:
                s = api_def.get_current_rate_limit(requests)
                if s:
                    out(s)

        retrieved = changed = checked = num_all_problems = 0
        for source_def, api_def in config.list_apps():
            if app_filter and app_filter not in source_def.app_id:
                continue

            checked += 1
            if offline:
                index_entry = self.get_entry(source_def.app_id)
            else:
                try:
                    tag_name, url, date = query_latest_release_asset(
                        cached_session, source_def, api_def
                    )
                except NoReleases as e:
                    out.red(f"NO RELEASES: {e.url}\n")
                    num_all_problems += 1
                    continue
                except TokenError as e:
                    out.red(f"403 HTTP Error during fetching: {e.url}\n")
                    num_all_problems += 1
                    continue

                old = self.get_entry(source_def.app_id)

                index_entry = IndexEntry(
                    app_id=source_def.app_id,
                    tag_name=tag_name,
                    url=url,
                    date=date,
                    source_code_url=source_def.source_code_url,
                    description=source_def.description,
                )
                if tag_name != getattr(old, "tag_name", None):
                    changed += 1

            cache_path = config.cache_dir.joinpath(index_entry.cache_relname)
            if True:
                s = f"got release: [{index_entry.app_id}] "
                s += " ".join([index_entry.tag_name, index_entry.date])
                out(s)
                out(f"        url: {index_entry.url}")
            if cache_path.exists():
                out(f"     cached: {cache_path}")
            elif not offline:
                r = cached_session.get(url)
                r.raise_for_status()
                cache_path.write_bytes(r.content)
                retrieved += 1
                out(f"     stored: {cache_path}", green=True)
            else:
                out.red("skipping download action because of offline run")
                out("")
                continue

            for problem in check_xdc_consistency(cache_path, source_def):
                out.red(problem)
                num_all_problems += 1

            with ZipFile(cache_path) as zipfile:
                manifest, _ = parse_manifest_and_icon(zipfile)

            index_entry.name = manifest["name"]
            self.set_entry(index_entry)
            out("")

        out.green(f"{checked} apps checked from {config.index_path}")
        out.green(f"{changed} apps had newer versions")
        out.green(f"{retrieved} '.xdc' release files retrieved")
        if num_all_problems > 0:
            out.red(f"{num_all_problems} problems found")
        else:
            out.green("all good, no problems detected in .xdc files")

    def perform_export(self, out):
        """Write all release files and the "xdcget.lock" application index file
        out to the the ini-configured `export_dir`.
        Removes all other files (but not directories) in the export directory.
        """
        self.config.export_dir.mkdir(exist_ok=True)
        cache_dir = self.config.cache_dir
        export_dir = self.config.export_dir

        written = []
        json_out = []
        num_apps = 0

        for entry in self.iter_entries():
            zip, icon = export_for_xdcstore(entry, cache_dir, export_dir)
            written.extend([zip, icon])
            json_entry = entry.__dict__.copy()
            json_entry["icon_relname"] = icon.name
            json_out.append(json_entry)
            num_apps += 1

        if num_apps == 0:
            out.red("No apps to export, probably a configuration error?")
            raise SystemExit(1)

        # write out json file
        dest_json = self.config.export_dir.joinpath("xdcget-lock.json")
        with dest_json.open("w") as f:
            json.dump(json_out, f, indent=2)
        written.append(dest_json)

        # only write xdcget.lock file after all release files are available/written
        dest = self.config.export_dir.joinpath(self.config.index_path.name)
        shutil.copy(self.config.index_path, dest)
        written.append(dest)

        # remove files or links not written by us, but don't remove dirs
        for path in self.config.export_dir.iterdir():
            if path not in written and path.is_file():
                out(f"removing from export-dir: {path}")
                path.unlink()

        out.green(
            f"Exported metadata and release files for {num_apps} apps to {dest.parent}"
        )


def query_latest_release_asset(session, source_def, api_def):
    """Return a (tag_name, url, date) tuple for the
    latest remotely available release asset.

    All three elememnts are string-tyuped, including `date`.

    Raises an exception if the remote release info can not be obtained.
    """
    latest_url = api_def.get_latest_release_url(source_def)
    r = session.get(latest_url, auth=api_def.auth)
    if r.status_code == 404:
        raise NoReleases(latest_url)
    if r.status_code == 403:
        raise TokenError(r)
    r.raise_for_status()

    json = r.json()
    tag_name = json["tag_name"]
    # we could look at lots of metadata in this json
    # but we just try grab the xdc file
    for asset in json["assets"]:
        if asset["name"].endswith(".xdc"):
            url = asset["browser_download_url"]
            date = asset["created_at"]
            return tag_name, url, date

    raise NoReleases(latest_url)
